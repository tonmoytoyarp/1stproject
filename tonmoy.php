<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning PHP</title>
    </head>
    <body>
        <h1>My Name Is Tonmoy</h1>
    <?php
        echo 'this is my first code of php';
        $var = 0;
        
        // Evaluates to true because $var is emtpy
        if (empty($var)) {
            echo '$var is either 0, empty, not set at all';
        }
        // Evaluates to true because $var is set
        if (isset($var)) {
            echo '$var is set even though it is empty';
        }
        
        $data = array(1, 1., NULL, new stdClass, 'foo');
        
        foreach ($data as $value) {
            echo gettype($value), "\n";
        }
        $yes = array('this', 'is', 'an array');
        
        echo is_array($yes)? 'Array' : 'not an Array';
        echo "\n";
        
        $no = 'this is a string';
        echo is_array($no)? 'Array' : 'not an Array';
        echo "\n";
        
        $a = false;
        $b = 0;
        
        // Since $a is a boolean, it will return true
        if (is_bool($a) === true) {
            echo "Yes, this is a boolean";
        }
        
        // Since $b is not a boolean, it will return false
        if (is_bool($b) === false) {
            echo "No, this is not a boolean";
        }
       
        error_reporting(E_ALL);
        
        $foo = NULL;
        var_dump(is_null($inexistent), is_null($foo));
        
        
    $expected_array_got_string = 'somestring';
    var_dump(empty($expected_array_got_string[0]));
    var_dump(empty($expected_array_got_string['0']));
    var_dump(empty($expected_array_got_string['some_key']));
    var_dump(empty($expected_array_got_string[0.5]));
    var_dump(empty($expected_array_got_string['0.5']));
    var_dump(empty($expected_array_got_string['0 Mostel']));
    
    // Declare a simple function to return an
    // array from out object
    function get_students($obj){
        if(!is_object($obj)) {
            return false;
        }
        
        return $obj->students;
    }
    
    // Declare a new class instance and fill up 
    // some values
    $obj = new stdClass();
    $obj->students = array('Kalle', 'Ross', 'Felipe');
    
    var_dump(get_students(null));
    var_dump(get_students($obj));
    
    $values = array(false, true, null, 'abc', '23', 23.5, '',' ', '0', 0);
    foreach ($values as $value) {
        echo "is_string(";
        var_export($value);
        echo ") = ";
        echo var_dump(is_string($value));
    }
    ?>

        
    </body>
</html>
